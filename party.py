# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import vobject
from trytond.model import ModelSQL, ModelView
from trytond.report import Report


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    def _vcard_n2values(self, party_id, vcard):
        res = {}
        if not hasattr(vcard, 'n') or not str(vcard.n.value).strip():
            if not hasattr(vcard, 'fn') or not str(vcard.fn.value).strip():
                if hasattr(vcard, 'org'):
                    res['name'] = vcard.org.value
            else:
                res['name'] = vcard.fn.value
            res['party_type'] = 'organization'
        else:
            res['name'] = vcard.n.value.family
            res['first_name'] = vcard.n.value.given
            res['party_type'] = 'person'
        return res

Party()


class VCard(Report):
    _name = 'party_vcarddav.party.vcard'

    def _party_name2vcard_n_values(self, party):
        res = super(VCard, self)._party_name2vcard_n_values(party)
        if party.party_type == 'person' and party.first_name:
            res['given'] = party.first_name
        elif party.party_type == 'organization':
            del res['family']
        return res

    def create_vcard(self, party):
        vcard = super(VCard, self).create_vcard(party)
        if party.party_type == 'organization':
            # Show party as company on iOS
            if not hasattr(vcard, 'X-ABSHOWAS'):
                vcard.add('x-abshowas')
            vcard.x_abshowas.value = 'COMPANY'
            if not hasattr(vcard, 'org'):
                vcard.add('org')
            vcard.org.value = [party.name,]
        return vcard

VCard()
