# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Type vCardDAV',
    'name_de_DE': 'Parteien Typen vCardDAV',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds support of types for parties with vCardDAV
    ''',
    'description_de_DE': '''
    - Fügt Unterstützung von Parteitypen mit vCardDAV hinzu.
    ''',
    'depends': [
        'party_type',
        'party_vcarddav',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
